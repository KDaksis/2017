<?php

class VideoOnDemand
{
    /**
     * @param $movie
     * Decode json fields, lookup for missing data
     * @since 1.4
     */
    public static function movie(&$movie)
    {
        $movie->actor = json_decode($movie->actor);
        $movie->director = json_decode($movie->director);
        $movie->languages = json_decode($movie->languages);

        if (strlen($movie->picture_large) == 0) {
            $movie->picture_large = $movie->picture_large_original ?? null;
        }

        $resourceType = ItemType::oldSchemaTypeToNewMapping($movie->type);
        $queryFields = Api::getQueryParameters()->getFieldSets()[$resourceType] ?? null;

        if (self::needToAddQuality($queryFields)
            && isset($movie->qualities) && json_decode($movie->qualities) != null) {
            $movie->quality = StreamQuality::streamQuality(json_decode($movie->qualities));
        }

        if (self::needToAddDefaultQuality($queryFields)
            && isset($movie->qualities) && json_decode($movie->qualities) != null) {
            $streamQualities = StreamQuality::defaultStreamQuality(json_decode($movie->qualities));
        }

        if (isset($streamQualities) && count($streamQualities) > 0) {
            $movie->default_quality = $streamQualities[0];
        } else {
            $movie->default_quality = null;
        }

        $movie->title = preg_replace('/\(HD\)[ ]*/', '', $movie->title, 1);
        $movie->title_original = preg_replace('/\(HD\)[ ]*/', '', $movie->title_original, 1);

        $db = JFactory::getDbo();
        $lang = JFactory::getLanguage();
        $query = $db->getQuery(true);
        $query
            ->select($db->quoteName('g.title_original'))
            ->select("coalesce(NULLIF(g.title, ''), g.title_original) as  title")
            ->from($db->quoteName('#__vod_genre', 'g'))
            ->where($db->quoteName('g.lang') . '=' . $db->quote($lang->getTag()))
            ->where($db->quoteName('g.state') . ' = 1 ');
        $db->setQuery($query);
        $genreTranslations = $db->loadAssocList("title_original", "title");

        $imdbGenres = json_decode($movie->imdb_genre);
        $imdbGenresTranslated = null;

        if ($imdbGenres != null) {
            foreach ($imdbGenres as $imdbGenre) {
                $imdbGenresTranslated[] = $genreTranslations[$imdbGenre] ?? '';
            }
        }

        $movie->imdb_genre = $imdbGenresTranslated;

        if (self::needToAddLanguage($queryFields)
            || self::needToAddDefaultLanguage($queryFields)) {
            $movie->languages = ($movie->languages)
                ? StreamLanguage::streamLanguages($movie->languages) : [];
        } else {
            $movie->languages = null;
        }

        $userData = null;
        if (self::needToAddDefaultLanguage($queryFields)
            || self::needToAddDefaultSubtitleLanguage($queryFields)) {
            $app = JFactory::getApplication();
            $input = $app->input;
            $token = json_decode($input->getString('tokendata'));
            $userId = $token->user_id ?? null;
            if ($userId) {
                $profile = $token->profile ?? (Profile::firstProfile($userId)->id);
            }
            $query = $db->getQuery(true);
            if ($userId != null) {
                $query->select('*')
                    ->from('#__user_settings')
                    ->where($db->quoteName('profile_id') . ' = ' . $db->quote($profile));
                $db->setQuery($query);
                $user = $db->loadObject();
            }
        }

        if (self::shallIAddThis($queryFields, "default-language")) {

            $ordering = 0;
            $breakLoop = false;
            $streamLanguages = [];
            if ($user != null) {
                $userLanguages = json_decode($user->lang);
                foreach ($userLanguages as $userLanguage) {
                    foreach ($movie->languages as $key => $language) {
                        if (trim($userLanguage) == $language->id) {
                            $language->ordering = (string)$ordering;
                            $ordering++;
                            $streamLanguages[] = $language;
                            $breakLoop = true;
                            break;
                        }
                    }
                    if ($breakLoop) {
                        break;
                    }
                }
            }
        }

        if (isset($streamLanguages[0])) {
            $movie->deflang = $streamLanguages[0];
        } elseif (empty($streamLanguages) && isset($movie->languages[0])) {
            $movie->deflang = $movie->languages[0];
        } else {
            $movie->deflang = null;
        }

        if (self::needToAddSubtiltes($queryFields)
            || self::needToAddDefaultSubtitleLanguage($queryFields)) {

            $subtitles = json_decode($movie->subtitles);

            $subtitleObj = new Subtitle();
            if (count($subtitles) > 0) {
                $movie->subtitles = $subtitleObj::getSubtitleLanguages($subtitles);
                $subtitles = array_merge($subtitles, ['-']);
            } else {
                $movie->subtitles = null;
                $subtitles = ['-'];
            }
            if (self::needToAddDefaultSubtitleLanguage($queryFields)) {
                $movie->sdeflang = $subtitleObj::defaultSubtitle($subtitles, $user);
            } else {
                $movie->sdeflang = null;
            }
        }

        if (isset($movie->seasons) && $movie->seasons) {
            $movie->seasons = explode(",", $movie->seasons);
        } elseif (isset($movie->seasons) && $movie->seasons === "0") {
            $movie->seasons = [$movie->seasons];
        }

        if (self::needToAddActualEpisode($queryFields, 'actual-episode')) {
            if (isset($movie->actual_episode) && $movie->actual_episode === $movie->movie_id) {
                $movie->actual_episode = $movie;
            } elseif (isset($movie->actual_episode)) {
                $movie->actual_episode = Vod::item($movie->actual_episode,
                    (isset($movie->recurrsion) ? $movie->recurrsion : null))[0];
            }
        }
    }
}